<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OOP</title>
</head>
<body>
    <h1> Belajar OOPHP </h1>
    <?php

        require_once "animal.php";
        require_once "frog.php";
        require_once "ape.php";

        $sheep = new Animal("shaun");

        echo "name = $sheep->name"; // "shaun"
        echo "<br>";
        echo "legs = $sheep->legs"; // 2
        echo "<br>";
        echo "cold-blooded = $sheep->cold_blooded"; // false
        echo "<br><br>";

        $sungokong = new Ape("kera sakti");

        echo "name = $sungokong->name"; 
        echo "<br>";
        echo "legs = $sungokong->legs"; 
        echo "<br>";
        echo "cold-blooded = $sungokong->cold_blooded"; 
        echo "<br>";
        $sungokong->yell(); // "Auooo"
        echo "<br><br>";

        $kodok = new Frog("buduk");
        
        echo "name = $kodok->name"; 
        echo "<br>";
        echo "legs = $kodok->legs"; 
        echo "<br>";
        echo "cold-blooded = $kodok->cold_blooded"; 
        echo "<br>";
        $kodok->jump() ; // "hop hop"

?>
</body>
</html>

